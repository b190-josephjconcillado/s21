console.log("Hello World!");
/** 
 * 98.5,94.3,89.2,90.1
 * arrays are used to store multiple related values in a single variable
 * arrays also provide access to a number of functions/ methods that help in achieving tasks that we can perform inside 
 * the arrays
 */
let grades = [98.5,94.3,89.2,90.1];
console.log(grades);
/**it is important that we store related values inside an array so that its variable can live up to its description of its value */
let computerBrands = ["Lenovo","Dell","Asus","HP","MSI","Acer","CDR-King"];
console.log(computerBrands);

let mixedArr = [12,"Asus",null,undefined];
console.log(mixedArr);

console.log("Array before reassigning: ");
console.log(mixedArr);
mixedArr[0] = "Hello World";
console.log("Array after reassigning: ");
console.log(mixedArr);

// SECTION - reading from arrays
console.log(grades);
console.log(grades[0]);
console.log(computerBrands[7]);

// Length
if(computerBrands.length > 5){
    console.log("We have too many suppliers. Please coordinate with the operations manager.");
}
let lastElementIndex = computerBrands.length -1;
console.log(computerBrands[lastElementIndex]);

let fruits = ["Apple","Mango","Rambutan","Lanzones","Durian"];
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
fruits.push("Orange");
console.log(fruits);

// Pop removes the element at the end of an array
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from push method");
console.log(fruits);

fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from push method");
console.log(fruits);

// unshift adds element at the start of an array
fruits.unshift("Lime","Banana");
console.log("Mutated Array from unshift method");
console.log(fruits);

// shift removes the element from the start of an array
let fruitRemoved = fruits.shift();
console.log("Mutated Array from shift method");
console.log(fruits);

// splice simultaneously removes and adds elements from specified index number.
// syntax arrayName.splice(startingIndex,deleteCount,elementsToBeAdd)
fruits.splice(1,2,"Lime","Cherry");
console.log("Mutated Array from slice method");
console.log(fruits);

// sort alphabetical order
fruits.sort();
console.log("Mutated Array from sort method");
console.log(fruits);

// reverse 
fruits.reverse();
console.log("Mutated Array from reverse method");
console.log(fruits);

/**
 * NON - MUtator methods
 * are functions that do not modify the original array
 * these methods do not manipulate the elements inside the array even they are performing tasks such as returning elements from an
 * array and combining them with other arrays and printing the output
 * indexOf()
 */
let countries = ["PH","RUS","CH","JPN","USA","KOR","AUS","CAN","PH"];
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: "+firstIndex);
firstIndex = countries.indexOf("KAZ")
console.log("Result of indexOf: "+firstIndex);

// lastindexOf()
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf: "+lastIndex);
lastIndex = countries.indexOf("KAZ")
console.log("Result of indexOf: "+lastIndex);

// slice()
let slicedArrayA = countries.slice(2);
console.log("Result of slice: "+slicedArrayA);
console.log(countries);

let slicedArrayB = countries.slice(2,7);
console.log("Result of slice: "+slicedArrayB);
console.log(countries);

let slicedArrayC = countries.slice(-3);
console.log("Result of slice: "+slicedArrayC);
console.log(countries);

// toString()
let stringArray = countries.toString();
console.log("Result of toString: "+stringArray);
console.log(stringArray);
console.log(countries);

// concat() used to combine two arrays and returns the combined result
// syntax: arrayA.concat(arrayB);
let tasksA = ["drink HTML", "eat Javascript"];
let tasksB = ["inhale CSS", "breathe SASS"];
let tasksC = ["get GIT","be node"];

let tasks = tasksA.concat(tasksB);
console.log("Result of concat: ");
console.log(tasks);

// combining multiple arrays
let allTasks = tasksA.concat(tasksB,tasksC);
console.log("Result of concat: ");
console.log(allTasks);

// join() returns an array as string separated by specified string separator
let users = ["John", "Jane", "Joe", "Jobert","Julius"];
console.log(users.join());
console.log(users.join(' '));
console.log(users.join('-'));

/*
     iterator methods 
     iteraton methods are loops designed to perform repetitive tasks on arrays 
     useful for manipulating array data resulting in complex tasks
*/

//forEach
/**
 * similar to for loop that loops through all elements
 * variable names for arrays are usally written in plural form of the data stored in an array
 * it is common practice to use the singular fomr of the array content for parameter names used in array loops
 * 
 * syntax:
 *      arrayName.forEach(function(individualElement)){
 *          statement/s    
 *      }
 */
allTasks.forEach(function(task){
    console.log(task);
});

// forEach with conditional statements
let filteredTasks = [];
allTasks.forEach(function(task){
    if(task.length > 10){
        filteredTasks.push(task);
    }
});
console.log("Result of forEach: ");
console.log(filteredTasks);

// map itereates on each elemnt and returns a new array with different values depending on the result of the funcions operation

let numbers = [1,2,3,4,5];
let numbersMap = numbers.map(function(number){
    return number*number;
});
console.log("Result of map: ");
console.log(numbers);
console.log(numbersMap);

// every()
/**
 * checks if all elements pass the given condition
 * returns a boolean data type depending if all elements meet the condition true or not (false)
 * snytax:
 * let/const result = arrayName.every(function(individualElement))
 */

let allValid = numbers.every(function(number){
    return (number < 3);
});
console.log("Result of every: " );
console.log(allValid);

// some()
let someValid = numbers.some(function(number){
    return (number < 2);
});
console.log("Result of some: ");
console.log(someValid);
console.log(numbers);

// filter()
/**
 * returns a new array that contains copies of the elements which meet the given condition 
 * returns empty array if there are no elements that meet the condtion
 * useful for filtering array elements with a given condition 
 * and shortens the syntax compared to using other array iteration method such as forEach + if statement + push in an earlier example
 * it is important that we make our work as efficient as possible
 */
let filterValid = numbers.filter(function(number){
    return (number < 3);
})
console.log("Result of filter: ");
console.log(filterValid);
console.log(numbers);